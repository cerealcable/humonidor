DHT Sensors
===========

## Types
| Type    | Description                                                   | Purchase Link                        |
|---------|---------------------------------------------------------------|--------------------------------------|
| DHT11   | Basic, ultra low-cost digital temperature and humidity sensor | https://www.adafruit.com/product/386 |
| DHT22   | Basic, low-cost digital temperature and humidity sensor       | https://www.adafruit.com/product/385 |
| AM2302  | Same as DHT22                                                 | https://www.adafruit.com/product/393 |

## Configuration Elements
| Key     | Description   | Value Type        | Required |
|---------|---------------|-------------------|----------|
| pin     | GPIO Pin      | Integer           | Yes      |

## Examples
Simple
```
    {"type": "AM2302", "pin": 4}
```

Another type
```
    {"type": "DHT22", "pin": 5}
```
