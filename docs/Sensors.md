Sensors
=======

| Type     | Documentation                | Overview                                      |
|----------|------------------------------|-----------------------------------------------|
| DHT      | [DHT.md](sensors/DHT.md)     | Inexpensive but and moderately accurate       |
| SHT3X    | [SHT3X.md](sensors/SHT3X.md) | Expensive but accurate                        |
