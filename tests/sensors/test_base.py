import pytest
from unittest.mock import MagicMock

from humonidor.sensors.base_sensor import BaseSensor


class TestSensor(BaseSensor):

    def get(self):
        super().get()
        return {}

    def get_temperature(self):
        super().get_temperature()
        return 0

    def get_humidity(self):
        super().get_humidity()
        return 0


def test_base_sensor():
    with pytest.raises(TypeError):
        instance = BaseSensor()

def test_test_sensor():
    instance = TestSensor()
    instance.get()
    instance.get_temperature()
    instance.get_humidity()
