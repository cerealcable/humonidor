import pytest
from unittest.mock import MagicMock
from unittest.mock import patch

from humonidor.sensors.dht import (
    AM2302, DHT11, DHT22,
)

@pytest.fixture(params=[0, 0.0, 30.0, 50, 100.0])
def t(request):
    return request.param

@pytest.fixture(params=[0, 0.0, 30.0, 50, 100.0])
def h(request):
    return request.param

@patch('Adafruit_DHT.read_retry')
def test_am2302(ada_read, t, h):
    ada_read.return_value = (h, t)
    sensor = AM2302(pin=1)
    assert str(sensor) == "<AM2302 pin=1>"
    assert sensor.get() == {'temperature': t, 'humidity': h}
    assert sensor.get_humidity() == h
    assert sensor.get_temperature() == t

@patch('Adafruit_DHT.read_retry')
def test_am2302_none(ada_read):
    ada_read.return_value = (None, None)
    sensor = AM2302(pin=1)
    assert str(sensor) == "<AM2302 pin=1>"
    assert sensor.get() == {}
    assert sensor.get_humidity() == None
    assert sensor.get_temperature() == None

@patch('Adafruit_DHT.read_retry')
def test_dht11(ada_read, t, h):
    ada_read.return_value = (h, t)
    sensor = DHT11(pin=1)
    assert str(sensor) == "<DHT11 pin=1>"
    assert sensor.get() == {'temperature': t, 'humidity': h}
    assert sensor.get_humidity() == h
    assert sensor.get_temperature() == t

@patch('Adafruit_DHT.read_retry')
def test_dht11_none(ada_read):
    ada_read.return_value = (None, None)
    sensor = DHT11(pin=1)
    assert str(sensor) == "<DHT11 pin=1>"
    assert sensor.get() == {}
    assert sensor.get_humidity() == None
    assert sensor.get_temperature() == None

@patch('Adafruit_DHT.read_retry')
def test_dht22(ada_read, t, h):
    ada_read.return_value = (h, t)
    sensor = DHT22(pin=1)
    assert str(sensor) == "<DHT22 pin=1>"
    assert sensor.get() == {'temperature': t, 'humidity': h}
    assert sensor.get_humidity() == h
    assert sensor.get_temperature() == t

@patch('Adafruit_DHT.read_retry')
def test_dht22_none(ada_read):
    ada_read.return_value = (None, None)
    sensor = DHT22(pin=1)
    assert str(sensor) == "<DHT22 pin=1>"
    assert sensor.get() == {}
    assert sensor.get_humidity() == None
    assert sensor.get_temperature() == None

