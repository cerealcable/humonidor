import json
import logging
import os
import shutil
import sys
import tempfile
import time
from unittest.mock import MagicMock
from unittest.mock import patch

import pytest

from humonidor import humonidor

@patch('Adafruit_IO.client.Client.send')
def test_humonidor(aio_send):
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':0, 'humidity':1}, datafile)
    datafile.close()
    h = humonidor.Humonidor(client_id='client_id', groups=False, name='test', sensors=[{'type': 'FILE', 'pathname': datafile.name}])
    h.poll()
    os.unlink(datafile.name)

@patch('Adafruit_IO.client.Client.send')
def test_humonidor_multiple_sensors(aio_send):
    datafile1 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':0, 'humidity':1}, datafile1)
    datafile1.close()

    datafile2 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':10, 'humidity':11}, datafile2)
    datafile2.close()

    h = humonidor.Humonidor(client_id='client_id', groups=False, name='test', sensors=[{'type': 'FILE', 'pathname': datafile1.name}, {'type': 'FILE', 'pathname': datafile2.name}])
    h.poll()

    os.unlink(datafile1.name)
    os.unlink(datafile2.name)

@patch('Adafruit_IO.client.Client.send')
def test_humonidor_group_multiple_sensors(aio_send):
    datafile1 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':0, 'humidity':1}, datafile1)
    datafile1.close()

    datafile2 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':10, 'humidity':11}, datafile2)
    datafile2.close()

    h = humonidor.Humonidor(client_id='client_id', groups=True, name='test', sensors=[{'type': 'FILE', 'pathname': datafile1.name}, {'type': 'FILE', 'pathname': datafile2.name}])
    h.poll()

    os.unlink(datafile1.name)
    os.unlink(datafile2.name)

@patch('Adafruit_IO.client.Client.send')
def test_humonidor_groups(aio_send):
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':0, 'humidity':1}, datafile)
    datafile.close()
    h = humonidor.Humonidor(client_id='client_id', groups=True, name='test', sensors=[{'type': 'FILE', 'pathname': datafile.name}])
    h.poll()
    os.unlink(datafile.name)

@patch('Adafruit_IO.client.Client.send')
def test_humonidor_without_data(aio_send):
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({}, datafile)
    datafile.close()
    h = humonidor.Humonidor(client_id='client_id', groups=True, name='test', sensors=[{'type': 'FILE', 'pathname': datafile.name}])
    h.poll()
    os.unlink(datafile.name)

@patch('Adafruit_IO.client.Client.send')
def test_humonidor_with_null_data(aio_send):
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature': None, 'humidity': None}, datafile)
    datafile.close()
    h = humonidor.Humonidor(client_id='client_id', groups=True, name='test', sensors=[{'type': 'FILE', 'pathname': datafile.name}])
    h.poll()
    os.unlink(datafile.name)

def test_humonidor_invalid_sensor():
    h2 = humonidor.Humonidor(client_id='client_id', groups=True, name='test', sensors=[{'type': 'fail', 'pin': 1}])

@patch('Adafruit_IO.client.Client.send')
def test_main(aio_send, monkeypatch):
    global counter_x
    counter_x = None

    def mytime(seconds):
        global counter_x
        if counter_x is None:
            counter_x = 1
        else:
            counter_x += 1
        if counter_x > 5:
            print("Raising KeyboardInterrupt")
            raise KeyboardInterrupt()
        print("Sleep")
        time.sleep(seconds)

    # sensor config
    sensor_data_file = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump({'temperature':0, 'humidity':1}, sensor_data_file)
    sensor_data_file.close()

    # main config
    config_data = {
      "adafruit-io": {
        "client-id": "12345",
        "groups": False
      },
      "humidors": [
        {
          "name": "Tupperdor 1",
          "sensors": [
            {"type":"FILE","pathname": sensor_data_file.name}
          ]
        }
      ],
      "interval": 10
    }
    config_file = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump(config_data, config_file)
    config_file.close()

    # log file
    log_dir = tempfile.mkdtemp()
    log_file = os.path.join(log_dir, 'humonidor.log')

    # run main
    with patch.object(sys, 'argv', ["humonidor", "-c", config_file.name, "-l", log_file, "--test", "-vvvv"]):
        print(sys.argv)
        humonidor.main()

    # patch time to raise KeyboardInterrupt after 5 calls
    monkeypatch.setattr(time, 'sleep', mytime)

    # run main
    with patch.object(sys, 'argv', ["humonidor", "-c", config_file.name, "-l", log_file, "-vvvv"]):
        print(sys.argv)
        humonidor.main()

    # Remove loggers to not interfere with future tests
    for name in logging.Logger.manager.loggerDict.keys():
        logger = logging.getLogger(name)
        for handler in logger.handlers[:]:
            logger.removeHandler(handler)
    logger = logging.getLogger()
    for handler in logger.handlers[:]:
        logger.removeHandler(handler)

    # cleanup
    shutil.rmtree(log_dir, ignore_errors=True)
    os.unlink(config_file.name)
    os.unlink(sensor_data_file.name)
