"""
 Humonidor
 =========
 A python humidor monitoring tool
 https://gitlab.com/tmyoungjr/humonidor/

 Copyright (c) 2017 Timothy Young, Morgan Humes
 Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

 Contributors by:
  - Timothy Young <tim@tmyoungjr.net>
  - Morgan Humes <morgan@lanaddict.com>
"""

# python
import time
from decimal import Decimal
import logging

# packages
import crcmod
from smbus2 import SMBus

# local
from .base_sensor import BaseSensor


class SHT3X(BaseSensor):
    crc8 = crcmod.mkCrcFun(0x131, initCrc=0xFF, rev=False, xorOut=0x00)

    # Commands
    CMD_READ_SERIALNBR  = (0x37, 0x80) # read serial number
    CMD_READ_STATUS     = (0xF3, 0x2D) # read status register
    CMD_CLEAR_STATUS    = (0x30, 0x41) # clear status register
    CMD_HEATER_ENABLE   = (0x30, 0x6D) # enabled heater
    CMD_HEATER_DISABLE  = (0x30, 0x66) # disable heater
    CMD_SOFT_RESET      = (0x30, 0xA2) # soft reset
    CMD_MEAS_CLOCKSTR_H = (0x2C, 0x06) # measurement: clock stretching, high repeatability
    CMD_MEAS_CLOCKSTR_M = (0x2C, 0x0D) # measurement: clock stretching, medium repeatability
    CMD_MEAS_CLOCKSTR_L = (0x2C, 0x10) # measurement: clock stretching, low repeatability
    CMD_MEAS_POLLING_H  = (0x24, 0x00) # measurement: polling, high repeatability
    CMD_MEAS_POLLING_M  = (0x24, 0x0B) # measurement: polling, medium repeatability
    CMD_MEAS_POLLING_L  = (0x24, 0x16) # measurement: polling, low repeatability
    CMD_MEAS_PERI_05_H  = (0x20, 0x32) # measurement: periodic 0.5 mps, high repeatability
    CMD_MEAS_PERI_05_M  = (0x20, 0x24) # measurement: periodic 0.5 mps, medium repeatability
    CMD_MEAS_PERI_05_L  = (0x20, 0x2F) # measurement: periodic 0.5 mps, low repeatability
    CMD_MEAS_PERI_1_H   = (0x21, 0x30) # measurement: periodic 1 mps, high repeatability
    CMD_MEAS_PERI_1_M   = (0x21, 0x26) # measurement: periodic 1 mps, medium repeatability
    CMD_MEAS_PERI_1_L   = (0x21, 0x2D) # measurement: periodic 1 mps, low repeatability
    CMD_MEAS_PERI_2_H   = (0x22, 0x36) # measurement: periodic 2 mps, high repeatability
    CMD_MEAS_PERI_2_M   = (0x22, 0x20) # measurement: periodic 2 mps, medium repeatability
    CMD_MEAS_PERI_2_L   = (0x22, 0x2B) # measurement: periodic 2 mps, low repeatability
    CMD_MEAS_PERI_4_H   = (0x23, 0x34) # measurement: periodic 4 mps, high repeatability
    CMD_MEAS_PERI_4_M   = (0x23, 0x22) # measurement: periodic 4 mps, medium repeatability
    CMD_MEAS_PERI_4_L   = (0x23, 0x29) # measurement: periodic 4 mps, low repeatability
    CMD_MEAS_PERI_10_H  = (0x27, 0x37) # measurement: periodic 10 mps, high repeatability
    CMD_MEAS_PERI_10_M  = (0x27, 0x21) # measurement: periodic 10 mps, medium repeatability
    CMD_MEAS_PERI_10_L  = (0x27, 0x2A) # measurement: periodic 10 mps, low repeatability
    CMD_FETCH_DATA      = (0xE0, 0x00) # readout measurements for periodic mode
    CMD_R_AL_LIM_LS     = (0xE1, 0x02) # read alert limits, low set
    CMD_R_AL_LIM_LC     = (0xE1, 0x09) # read alert limits, low clear
    CMD_R_AL_LIM_HS     = (0xE1, 0x1F) # read alert limits, high set
    CMD_R_AL_LIM_HC     = (0xE1, 0x14) # read alert limits, high clear
    CMD_W_AL_LIM_HS     = (0x61, 0x1D) # write alert limits, high set
    CMD_W_AL_LIM_HC     = (0x61, 0x16) # write alert limits, high clear
    CMD_W_AL_LIM_LC     = (0x61, 0x0B) # write alert limits, low clear
    CMD_W_AL_LIM_LS     = (0x61, 0x00) # write alert limits, low set
    CMD_NO_SLEEP        = (0x30, 0x3E)


    def __init__(self, bus, address, heater=False, **kwargs):
        """
        SHT3X sensor
        SHT3X address can be 0x44 or 0x45
        """
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))
        self.bus_address = bus
        if isinstance(address, str):
            self.address = int(address, 0)
        else:
            self.address = address
        self.bus = SMBus(self.bus_address)
        self.heater = heater

    def __repr__(self):
        return "<SHT3X bus={} address=0x{:02X}>".format(self.bus_address, self.address)

    def _write_command(self, cmd):
        self.logger.debug('write_command bus={} address=0x{:02X} cmd=0x{}'.format(self.bus_address, self.address, ''.join(['{:02X}'.format(x) for x in cmd])))
        try:
            self.bus.write_i2c_block_data(self.address, cmd[0], cmd[1:])
        except Exception as e:
            self.logger.exception('Unable to write to sensor!')
            raise e

    def _read_bytes(self, register, count):
        self.logger.debug('read_bytes bus={} address=0x{:02X} register=0x{:02X} count={}'.format(self.bus_address, self.address, register, count))
        try:
            return self.bus.read_i2c_block_data(self.address, register, count)
        except Exception as e:
            self.logger.exception('Unable to read from sensor!')
            raise e

    def _read_data(self):
        """
        Read raw data from sensor
        Read data back from 0x00(00), 6 bytes
        Temp MSB, Temp LSB, Temp CRC, Humididty MSB, Humidity LSB, Humidity CRC
        """
        try:
            self._write_command(SHT3X.CMD_MEAS_CLOCKSTR_H)
            time.sleep(0.5)
            data = self._read_bytes(0, 6)
            if data is None:
                return (None, None)
            t = (data[0] << 8) + data[1]
            t_hash = SHT3X.crc8(bytes([data[0], data[1]]))
            t_crc = data[2]
            h = (data[3] << 8) + data[4]
            h_hash = SHT3X.crc8(bytes([data[3], data[4]]))
            h_crc = data[5]

            if self.heater:
                # start heater
                self._write_command(SHT3X.CMD_HEATER_ENABLE)
                time.sleep(5)
                # stop heater
                self._write_command(SHT3X.CMD_HEATER_DISABLE)

                # re-read humidity
                self._write_command(SHT3X.CMD_MEAS_CLOCKSTR_H)
                time.sleep(0.5)
                data = self._read_bytes(0, 6)
                h = (data[3] << 8) + data[4]
                h_hash = SHT3X.crc8(bytes([data[3], data[4]]))
                h_crc = data[5]

            self.logger.debug('t: {} h: {}'.format(t, h))
            self.logger.debug('t_crc: {} t_hash: {} h_crc: {} h_hash: {}'.format(hex(t_crc), hex(t_hash), hex(h_crc), hex(h_hash)))

            if t_hash != t_crc or h_hash != h_crc:
                raise Exception("CRC Error")

            return (t, h)
        except Exception:
            self.logger.exception("Failed to read from sensor")
            return (None, None)

    def get(self):
        """
        Get and convert sensor data into usable values
        """
        t_raw, h_raw = self._read_data()
        if t_raw is not None and h_raw is not None:
            t_calc = Decimal(175.0 * t_raw / 65535.0 - 45)
            h_calc = Decimal(100.0 * h_raw / 65535.0)
            temperature = float(round(t_calc, 1))
            humidity = float(round(h_calc, 1))
            self.logger.info('temperature: {} humidity: {}'.format(temperature, humidity))
            return {
                'temperature': temperature,
                'humidity': humidity,
            }
        else:
            return {}

    def get_humidity(self):
        return self.get().get('humidity')
            
    def get_temperature(self):
        return self.get().get('temperature')

