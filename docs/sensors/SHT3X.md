SHT3X-D Sensors
===============

## Types

| Type    | Description                                                   | Purchase Link                        |
|---------|---------------------------------------------------------------|--------------------------------------|
| SHT31   | High Accuracy digital temperature and humidity sensor         | https://www.adafruit.com/product/2857 |
| SHT35   | Basic, low-cost digital temperature and humidity sensor       | https://www.tindie.com/products/closedcube/sht35-d-digital-humidity-and-temperature-sensor/ |


## Configuration Elements

| Key     | Description   | Value Type        | Required |
|---------|---------------|-------------------|----------|
| bus     | I2C Bus       | Integer           | Yes      |
| address | I2C Address   | String or Integer | Yes      |
| heater  | Enable heater | Boolean           | No       |


## Examples
Simple
```
    {"type": "SHT31", "bus": 1, "address": "0x44"}
```

With heater
```
    {"type": "SHT35", "bus": 1, "address": "0x44", "heater": true}
```
