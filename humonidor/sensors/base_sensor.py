"""
 Humonidor
 =========
 A python humidor monitoring tool
 https://gitlab.com/tmyoungjr/humonidor/

 Copyright (c) 2017 Timothy Young, Morgan Humes
 Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

 Contributors by:
  - Timothy Young <tim@tmyoungjr.net>
  - Morgan Humes <morgan@lanaddict.com>
"""
from abc import ABC, abstractmethod


class BaseSensor(ABC):
    """ Base Sensor Abstract Class """

    @abstractmethod
    def get(self):
        """ Return dictionary of sensor values """
        pass

    @abstractmethod
    def get_temperature(self):
        """ Return temperature value """
        pass

    @abstractmethod
    def get_humidity(self):
        """ Return humidity """
        pass
