.requirements:
	pip3 install -r requirements-dev.txt
	touch .requirements


.PHONY: install-packages
install-packages: .requirements


.PHONY: test
test: install-packages
	pytest -vvvx --cov --cov-report=term --cov-report=html --html=reports/pytest.html --self-contained-html
