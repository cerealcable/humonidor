"""
 Humonidor
 =========
 A python humidor monitoring tool
 https://gitlab.com/tmyoungjr/humonidor/

 Copyright (c) 2017 Timothy Young, Morgan Humes
 Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

 Contributors by:
  - Timothy Young <tim@tmyoungjr.net>
  - Morgan Humes <morgan@lanaddict.com>
"""
# python
import json
import logging

# local
from .base_sensor import BaseSensor


class File(BaseSensor):
    """ File Sensor """

    def __init__(self, pathname, **kwargs):
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))
        self.pathname = pathname

    def __repr__(self):
        return "<File Pathname={}>".format(self.pathname)

    def get(self):
        with open(self.pathname) as f:
            data = json.loads(f.read())
        self.logger.debug('raw: {}'.format(data))
        self.logger.info('temperature: {} humidity: {}'.format(data.get('temperature'), data.get('humidity')))
        return data

    def get_humidity(self):
        r = self.get()
        if r:
            return r.get('humidity')
        else:
            return None

    def get_temperature(self):
        r = self.get()
        if r:
            return r.get('temperature')
        else:
            return None
