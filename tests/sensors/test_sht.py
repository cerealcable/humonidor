# pytest
import pytest
from unittest.mock import MagicMock
from unittest.mock import patch

# python
import logging
import logging.handlers
import sys
import time

# humonidor
from humonidor.sensors.sht import SHT3X


@pytest.fixture(autouse=True)
def no_sleep(monkeypatch):
    monkeypatch.setattr(time, 'sleep', lambda s: None)

@pytest.fixture(params=["0x44", 0x44])
def address(request):
    return request.param

@pytest.fixture(params=[
    {'raw': ( 65, 211, 171), 'val': 0.0},
    {'raw': (109, 182, 141), 'val': 30.0},
    {'raw': (138, 248, 119), 'val': 50.0},
    {'raw': (212,  28, 248), 'val': 100.0},
])
def temp(request):
    return request.param

@pytest.fixture(params=[
    {'raw': (  0,   0, 129), 'val': 0.0},
    {'raw': ( 76, 204, 134), 'val': 30.0},
    {'raw': (127, 255, 143), 'val': 50.0},
    {'raw': (255, 255, 172), 'val': 100.0},
])
def humid(request):
    return request.param

@pytest.fixture(params=[True, False, None])
def heater(request):
    return request.param

class TestSHT3X:

    @classmethod
    def setup_class(cls):
        # Setup logging
        logger = logging.getLogger()
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s') 
        # stdout
        ch = logging.StreamHandler(sys.stdout)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        # level
        logger.setLevel(logging.DEBUG)

    @classmethod
    def teardown_class(cls):
        pass

    @patch('humonidor.sensors.sht.SMBus')
    def test_sht3x(self, mock_SMBus, address, heater, temp, humid):
        mock_SMBus.return_value.read_i2c_block_data.return_value = temp['raw'] + humid['raw']
        sensor = SHT3X(bus=1, address=address, heater=heater)
        assert str(sensor) == "<SHT3X bus=1 address=0x44>"
        assert sensor.get() == {'temperature': temp['val'], 'humidity': humid['val']}
        assert sensor.get_humidity() == humid['val']
        assert sensor.get_temperature() == temp['val']

    @patch('humonidor.sensors.sht.SMBus')
    def test_sht3x_crc_error(self, mock_SMBus, address, heater, temp, humid):
        t_raw = temp['raw']
        h_raw = humid['raw']
        mock_SMBus.return_value.read_i2c_block_data.return_value = (t_raw[0], t_raw[1], t_raw[2]+1, h_raw[0], h_raw[1], h_raw[1]+1)
        sensor = SHT3X(bus=1, address=address, heater=heater)
        assert str(sensor) == "<SHT3X bus=1 address=0x44>"
        assert sensor.get() == {}
        assert sensor.get_humidity() == None
        assert sensor.get_temperature() == None

    @patch('humonidor.sensors.sht.SMBus')
    def test_sht3x_none(self, mock_SMBus, heater):
        mock_SMBus.return_value.read_i2c_block_data.return_value = None
        sensor = SHT3X(bus=1, address=0x44, heater=heater)
        assert str(sensor) == "<SHT3X bus=1 address=0x44>"
        assert sensor.get() == {}
        assert sensor.get_humidity() == None
        assert sensor.get_temperature() == None

    @patch('humonidor.sensors.sht.SMBus')
    def test_sht3x_read_error(self, mock_SMBus):
        def mock_read_fn(*args, **kwargs):
            raise Exception("Test Exception")
        mock_SMBus.return_value.read_i2c_block_data = mock_read_fn
        sensor = SHT3X(bus=1, address=0x44, heater=heater)
        assert str(sensor) == "<SHT3X bus=1 address=0x44>"
        assert sensor.get() == {}

    @patch('humonidor.sensors.sht.SMBus')
    def test_sht3x_write_error(self, mock_SMBus):
        def mock_write_fn(*args, **kwargs):
            raise Exception("Test Exception")
        mock_SMBus.return_value.write_i2c_block_data = mock_write_fn
        sensor = SHT3X(bus=1, address=0x44, heater=heater)
        assert str(sensor) == "<SHT3X bus=1 address=0x44>"
        assert sensor.get() == {}

