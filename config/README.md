# Configuration

copy config/humonidor.json.sample to /etc/humonidor.json

## Configuration options

Adafruit.io settings

```
{
  "adafruit-io": {
    "client-id": "1aeb178618ba40779776cd1ad12e3fe9",
    "groups": false
```

set "client-id" to your AIO key listed on your main AIO page  
set "groups" to TRUE if you have groups setup within your AIO feed list

```
  "humidors": [
    {
      "name": "Tupperdor 1",
      "sensors": [
        {"type":"AM2302","pin":"4"}
```

set "name" to a unique identifier for the group of sensors.  you can have multiple name blocks  
for each sensor you have you can duplicate the type line as many times as needed  
set "type" to one of the following :  AM2302, DHT11, DHT22  
set "pin" to the GPIO pin used by that particular sensor  

```
  "interval": 10
```

set interval to how often you want the sensors read in minutes.  default is every 10 minutes

## Copyright and license
Copyright Timothy Young and Morgan Humes. Released under [the MIT license](https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE) unless otherwise specified.
```
