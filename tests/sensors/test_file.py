import pytest
import json
import os
import tempfile
from unittest.mock import MagicMock

from humonidor.sensors.file import File


def test_file():
    data = {'temperature':0, 'humidity':1}
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump(data, datafile)
    datafile.close()

    instance = File(pathname=datafile.name)
    assert str(instance) == "<File Pathname={}>".format(datafile.name)
    assert instance.get() == data
    assert instance.get_humidity() == data.get('humidity')
    assert instance.get_temperature() == data.get('temperature')
    os.unlink(datafile.name)


def test_file_no_data():
    data = {}
    datafile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    json.dump(data, datafile)
    datafile.close()

    instance = File(pathname=datafile.name)
    assert str(instance) == "<File Pathname={}>".format(datafile.name)
    assert instance.get() == data
    assert instance.get_humidity() == data.get('humidity')
    assert instance.get_temperature() == data.get('temperature')
    os.unlink(datafile.name)
