#!/usr/bin/python
"""
 Humonidor
 =========
 A python humidor monitoring tool
 https://gitlab.com/tmyoungjr/humonidor/

 Copyright (c) 2017 Timothy Young, Morgan Humes
 Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

 Contributors by:
  - Timothy Young <tim@tmyoungjr.net>
  - Morgan Humes <morgan@lanaddict.com>
"""
# python
import argparse
import json
import logging
import logging.handlers
import os
import statistics
import sys
import time

# packages
import Adafruit_IO
import schedule

# local
from humonidor import VERSION, sensors
from .thirdparty.patharg import PathType


class Humonidor():
    """
    Monitors a humidors environment
    """

    SENSOR_MAP = {
        'AM2302': sensors.AM2302,
        'DHT11':  sensors.DHT11,
        'DHT22':  sensors.DHT22,
        'SHT31':  sensors.SHT3X,
        'SHT35':  sensors.SHT3X,
        'FILE':   sensors.File,
    }

    def __init__(self, client_id, groups, name, sensors):
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))
        self._aio_client = Adafruit_IO.Client(client_id)
        self._name = name
        self._sensors = []
        for idx, sensor_cfg in enumerate(sensors):
            sensor_type = sensor_cfg.get('type')
            sensor_cls = Humonidor.SENSOR_MAP.get(sensor_type)
            if sensor_cls:
                sensor = sensor_cls(**sensor_cfg)
                self._sensors.append(sensor)
                self.logger.info('{}: Loaded sensor #{} as {}'.format(self._name, idx, sensor))
            else:
                self.logger.warning('{}: FAILED to load sensor #{} (given type "{}")'.format(self._name, idx, sensor_type))
        self._groups = groups
        if groups:
            self._humidity_key = "{}.humidity".format(name)
            self._temperature_key = "{}.temperature".format(name)
        else:
            self._humidity_key = "{}-humidity".format(name)
            self._temperature_key = "{}-temperature".format(name)

    def _get_keys(self, sensor_idx):
        """ Get temperature and humidity key names """
        prefix = "sensor{}-".format(sensor_idx)
        if self._groups:
            data = {
                "temperature": "{}.{}temperature".format(self._name, prefix),
                "humidity": "{}.{}humidity".format(self._name, prefix)
            }
        else:
            data = {
                "temperature": "{}-{}temperature".format(self._name, prefix),
                "humidity": "{}-{}humidity".format(self._name, prefix)
            }
        return data

    @staticmethod
    def _convert_to_f(temp):
        """ Simple temperature conversion from celcius to fahrenheit """
        return round(temp * 9/5.0 + 32, 1)

    def poll(self):
        """
        Poll sensors and update adafruit io
        """
        temperatures = []
        humidities = []
        post_sensors = True if len(self._sensors) > 1 else False
        for idx, sensor in enumerate(self._sensors):
            data = sensor.get()
            humidity = data.get('humidity')
            temperature = data.get('temperature')

            # validate data and publish
            if humidity is not None and temperature is not None:
                temperature = Humonidor._convert_to_f(temperature)
                temperatures.append(temperature)
                humidities.append(humidity)
                if post_sensors:
                    self.logger.info('{}: Sensor #{} Temp={:0.1f}° Humidity={:0.1f}%'.format(self._name, idx, temperature, humidity))
                    keys = self._get_keys(idx)
                    self.post_data(reading={
                        keys['temperature']: temperature,
                        keys['humidity']: humidity
                    })
            else:
                # if either of the readings come up empty print an error message to the logfile
                self.logger.error('Failed to get reading.  Try Again!')

        if temperatures and humidities:
            temperature_avg = round(statistics.mean(temperatures),1)
            humidity_avg = round(statistics.mean(humidities),1)
            self.logger.info('{}: Temp={:0.1f}° Humidity={:0.1f}%'.format(self._name, temperature_avg, humidity_avg))
            self.post_data(reading={
                self._temperature_key: temperature_avg,
                self._humidity_key: humidity_avg
            })
        else:
           self.logger.error('Missing a minimum of one data point for averages')

    def post_data(self, reading):
        """ Post data to adafruit io """
        for key, value in reading.items():
            self.logger.debug('Sending to AIO key: {} value: {}'.format(key, value))
            self._aio_client.send(key, value)

def main():
    """ Main entry point """
    # Parse args and configuration
    parser = argparse.ArgumentParser(description='Humonidor: Monitor a humidors temperature and humidity.')
    parser.add_argument('-c', '--config', required=True, type=argparse.FileType('r'), help='Configuration file')
    parser.add_argument('-l', '--log-file', type=PathType(exists=None, type='file', dash_ok=False), help='Log File')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('--test', dest='test', action='store_true', help='Test run one time and exit')

    args = parser.parse_args()
    config = json.load(args.config)

    # Setup logging
    logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s') 

    # stdout logging
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # file logging
    if args.log_file:
        ch = logging.handlers.RotatingFileHandler(filename=args.log_file, maxBytes=5000, backupCount=3)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    humonidor_logger = logging.getLogger('humonidor')
    sensors_logger = logging.getLogger('humonidor.sensors')

    logger.setLevel(logging.WARNING)
    humonidor_logger.setLevel(logging.INFO)
    sensors_logger.setLevel(logging.WARNING)
    if args.verbose >= 1:
        logger.setLevel(logging.INFO)
        humonidor_logger.setLevel(logging.DEBUG)
    if args.verbose >= 2:
        sensors_logger.setLevel(logging.INFO)
    if args.verbose >= 3:
        sensors_logger.setLevel(logging.DEBUG)
    if args.verbose >= 4:
        logger.setLevel(logging.DEBUG)

    humonidor_logger.info('Humonidor Version {}'.format(VERSION))

    # Register each Humidor to monitor
    adafruit_id = config['adafruit-io']['client-id']
    adafruit_groups = config['adafruit-io'].get('groups', False)
    for humidor_config in config['humidors']:
        humidor = Humonidor(client_id=adafruit_id, groups=adafruit_groups,
                            name=humidor_config['name'], sensors=humidor_config['sensors'])
        # First reading
        humidor.poll()
        # Schedule future readings
        schedule.every(config['interval']).minutes.do(humidor.poll)

    # Run schedule
    run = True
    while run:
        try:
            schedule.run_pending()
            time.sleep(1)
            if args.test:
                run = False
        except KeyboardInterrupt:
            print("Stopping!")
            run = False

if __name__ == "__main__":
    main()
