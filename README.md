# Humonidor
Monitoring the humidity and temperature of various humidors / tupperdors with a Raspberry Pi (Model 3B) and DHT11, DHT22 or AM2302 sensors.

## Requirements
* Raspberry Pi (for now)
* Temperature Sensor (DHT11, DHT22 or AM2302 for now)
* Adafruit IO account
* Python3

## Adafruit.io
https://io.adafruit.com is used to receive and store data.  Create an account there.  You'll need the AIO key.

## Installation
See [Install.md](docs/Install.md) for getting Humonidor running with systemd on a Raspberry Pi 3!

## Manual Execution
Simply run `humonidor`, passing in the `--help` flag will give a synopsis of command line arguments.
```
/usr/local/bin/humonidor -h
usage: humonidor [-h] -c CONFIG [-l LOG_FILE] [--verbose] [--test]

Humonidor: Monitor a humidors temperature and humidity.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        Configuration file
  -l LOG_FILE, --log-file LOG_FILE
                        Log File
  --verbose, -v
  --test                Test run one time and exit
```

## Copyright and license
Copyright Timothy Young and Morgan Humes. Released under [the MIT license](https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE) unless otherwise specified.
