# Humonidor installation on a Raspberry Pi 3
Follow this and you'll have a working Raspberry Pi 3 with Humonidor!

## Requirements
* Raspberry Pi 3 (or similar)
* Raspbian Stretch
* Language set to support UTF8

## Setup
```
sudo apt-get install git build-essential python3-dev python3-rpi.gpio
git clone https://github.com/adafruit/Adafruit_Python_DHT
cd Adafruit_Python_DHT
sudo python3 setup.py install
cd -
git clone https://gitlab.com/tmyoungjr/humonidor/
cd humonidor
sudo python3 setup.py install
```
You can now run `humonidor` in your shell. It is required to have a configuration file and to specifiy it with the `--config` flag.

## Configuration
Reference config/humonidor.json.sample to create your own configuration. See the specific documentation for [sensors](docs/Sensors.md) in the [docs](docs/) folder in order to properly configure each sensor and the attributes it needs.

## Systemd
If you'd like to manage Humonidor with systemd (default in Raspbian Stretch), do the following. Your configuration will need to be in a file `/etc/humonidor.json` for the systemd service to run.
```
cp contrib/humonidor.service /etc/systemd/system/
sudo mkdir /var/log/humonidor
sudo chown pi: /var/log/humonidor
sudo systemctl daemon-reload
sudo systemctl start humonidor.service
```
