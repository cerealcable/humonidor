"""
 Humonidor
 =========
 A python humidor monitoring tool
 https://gitlab.com/tmyoungjr/humonidor/

 Copyright (c) 2017 Timothy Young, Morgan Humes
 Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

 Contributors by:
  - Timothy Young <tim@tmyoungjr.net>
  - Morgan Humes <morgan@lanaddict.com>
"""
# python
from decimal import Decimal
import logging

# packages
import Adafruit_DHT

# local
from .base_sensor import BaseSensor


class DHTBase(BaseSensor):
    """ DHT Base Class """

    def __init__(self, _type, pin, **kwargs):
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))
        self.type = _type
        self.pin = pin

    def get(self):
        h_raw, t_raw = Adafruit_DHT.read_retry(self.type, self.pin)
        if h_raw is not None and t_raw is not None:
            self.logger.debug('t_raw: {} h_raw: {}'.format(t_raw, h_raw))
            h_calc = Decimal(h_raw)
            t_calc = Decimal(t_raw)
            humidity = float(round(h_calc, 1))
            temperature = float(round(t_calc, 1))
            self.logger.info('temperature: {} humidity: {}'.format(temperature, humidity))
            return {
                'temperature': temperature,
                'humidity': humidity
            }
        else:
            return {}

    def get_humidity(self):
        return self.get().get('humidity')

    def get_temperature(self):
        return self.get().get('temperature')


class AM2302(DHTBase):
    """ AM2302 Sensor """

    def __init__(self, pin, **kwargs):
        super().__init__(Adafruit_DHT.AM2302, pin, **kwargs)
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))

    def __repr__(self):
        return "<AM2302 pin={}>".format(self.pin)


class DHT11(DHTBase):
    """ DHT11 Sensor """

    def __init__(self, pin, **kwargs):
        super().__init__(Adafruit_DHT.DHT11, pin, **kwargs)
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))

    def __repr__(self):
        return "<DHT11 pin={}>".format(self.pin)


class DHT22(DHTBase):
    """ DHT22 Sensor """

    def __init__(self, pin, **kwargs):
        super().__init__(Adafruit_DHT.DHT22, pin, **kwargs)
        self.logger = logging.getLogger('{}.{}'.format(__name__,self.__class__.__name__))

    def __repr__(self):
        return "<DHT22 pin={}>".format(self.pin)

