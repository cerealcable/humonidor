"""
  Humonidor
  =========
  A python humidor monitoring tool
  https://gitlab.com/tmyoungjr/humonidor/

  Copyright (c) 2017 Timothy Young, Morgan Humes
  Licensed under MIT (https://gitlab.com/tmyoungjr/humonidor/blob/license/LICENSE)

  Contributors by:
    - Timothy Young <tim@tmyoungjr.net>
    - Morgan Humes <morgan@lanaddict.com>
"""
from setuptools import setup, find_packages
from codecs import open
from os import path

from humonidor import VERSION


setup(
    name='humonidor',
    version=VERSION,
    description='A Humidor Monitoring Service',
    url='https://gitlab.com/tmyoungjr/humonidor',
    install_requires=[
        'adafruit-io',
        'paho-mqtt',
        'python-etcd',
        'schedule>=0.5.0',
        'smbus2',
        'crcmod',
    ],
    scripts=['scripts/humonidor'],
    packages=find_packages()
)
